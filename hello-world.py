import logging
from numerous.image_tools.job import NumerousBaseJob, ExitCode
from numerous.image_tools.app import run_job

logger = logging.getLogger(__name__)

class HelloWorld(NumerousBaseJob):
    def run_job(self):
        logger.warning("hello world 2")
        return ExitCode.COMPLETED


if __name__ == "__main__":
    run_job(numerous_job=HelloWorld(), appname="hello-world-job", model_folder=None)